import { MorderFrontendPage } from './app.po';

describe('morder-frontend App', function() {
  let page: MorderFrontendPage;

  beforeEach(() => {
    page = new MorderFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
