import { Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser'
import { TokenService } from './token.service';



@Component({
  selector: 'login-jumbotron',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  token: string;
  token_expires_at;
  redirect_uri: string;
  hashParameters={};

  constructor(@Inject(DOCUMENT) private document: any) {
    console.log("We're at "+ this.document.location.href);
    this.redirect_uri =  this.document.location.href;
    let parametersArray = this.document.location.href.split("#")[1].split("&");
    for (let parameter of parametersArray) {
      this.hashParameters[parameter.split("=")[0]] = parameter.split("=")[1];
    }
    this.token = this.hashParameters["access_token"];
    let current_date = Date();
    this.token_expires_at = new Date(Date.now() + Number(this.hashParameters["expires_in"])*1000);
  }


}
